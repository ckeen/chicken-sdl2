
;;; chicken-sdl2 test suite entry point.

(use (prefix sdl2 sdl2:)
     (prefix sdl2-internals SDL:)
     test)


(include "test-helpers.scm")


(define Uint8-min   0)
(define Uint8-max   255)
(define Sint32-min -2147483648)
(define Sint32-max  2147483647)


(test-group "sdl2"
  (test-group "color"         (include "color-tests.scm"))
  (test-group "hints"         (include "hints-tests.scm"))
  (test-group "keysym"        (include "keysym-tests.scm"))
  (test-group "mouse-wheel-event"
    (include "mouse-wheel-event-tests.scm"))
  (test-group "point"         (include "point-tests.scm"))
  (test-group "rect"          (include "rect-tests.scm"))
  (test-group "render"        (include "render-tests.scm"))
  (test-group "struct"        (include "struct-tests.scm"))
  (test-group "surface"       (include "surface-tests.scm"))
  (test-group "window"        (include "window-tests.scm"))
  )


(test-exit)
